package ball;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.jbox2d.common.Vec2;

public class PlayState extends BasicGameState implements ClickListener {

    private LevelLoader loader;
	private Level currentLevel;
	private Input in;
	private UserInterface ui;
	
	private MoveableBeam newBeam = null;
	private boolean placingBeam = false;
	
	public MoveableBeam Marked = null;
	public GameContainer gcsave;
	
    public PlayState() {

    }
    
    private void disableButtons() {
    	if (ui.getButton("CreateBeam") != null) 
    		ui.getButton("CreateBeam").unregister(this);
    	if (ui.getButton("CreateBeamlarge") != null) 
    		ui.getButton("CreateBeamlarge").unregister(this);
    	if (ui.getButton("CreateBeamnormal") != null)
    		ui.getButton("CreateBeamnormal").unregister(this);
    }

    private void enableButtons() {
    	if (ui.getButton("CreateBeam") != null) 
    		ui.getButton("CreateBeam").register(this);
    	if (ui.getButton("CreateBeamlarge") != null) 
    		ui.getButton("CreateBeamlarge").register(this);
    	if (ui.getButton("CreateBeamnormal") != null)
    		ui.getButton("CreateBeamnormal").register(this);
    }
    @Override
    public void click(Button button) {
    	try {
    		switch (button.getName()) {
    			case "CreateBeam":
    				if (!currentLevel.CheckLost() && currentLevel.smallBeamLimit > 0){
	    				newBeam = new SmallBeamMoveable(
	    						false, 0, new Vec2(in.getMouseX(), in.getMouseY() - currentLevel.GetVerschiebung()),
	    						currentLevel);
						Marked = newBeam;
	    				placingBeam = true;
	    				if (currentLevel.smallBeamLimit != 100){
	    					currentLevel.smallBeamLimit -= 1;
	    					if (currentLevel.smallBeamLimit == 0){
	    						ui.addButton("res/buttons/psCreateBeam2.png", "DontCreateBeam",
	    				        		button.getX(), button.getY(), 50, 50);
	    						ui.removeButton("CreateBeam");
	    					}
	    				}
    				}
    				disableButtons();
    				break;
    			case "CreateBeamnormal":
    				if (!currentLevel.CheckLost() && currentLevel.normalBeamLimit > 0){
	    				newBeam = new NormalBeamMoveable(
								false, 0, new Vec2(in.getMouseX(), in.getMouseY() - currentLevel.GetVerschiebung()),
	    						currentLevel);
						Marked = newBeam;
	    				placingBeam = true;
	    				if (currentLevel.normalBeamLimit != 100){
	    					currentLevel.normalBeamLimit -= 1;
	    					if (currentLevel.normalBeamLimit == 0){
	    						ui.addButton("res/buttons/psCreateBeamnormal2.png", "DontCreateBeam",
	    				        		button.getX(), button.getY(), 50, 50);
	    						ui.removeButton("CreateBeamnormal");
	    					}
	    				}
    				}
    				disableButtons();
    				break;
    			case "CreateBeamlarge":
    				if (!currentLevel.CheckLost() && currentLevel.largeBeamLimit > 0){
	    				newBeam = new LargeBeamMoveable(
								false, 0, new Vec2(in.getMouseX(), in.getMouseY() - currentLevel.GetVerschiebung()),
	    						currentLevel);
						Marked = newBeam;
	    				placingBeam = true;
	    				if (currentLevel.largeBeamLimit != 100){
	    					currentLevel.largeBeamLimit -= 1;
	    					if (currentLevel.largeBeamLimit == 0){
	    						ui.addButton("res/buttons/psCreateBeamlarge2.png", "DontCreateBeam",
	    				        		button.getX(), button.getY(), 50, 50);
	    						ui.removeButton("CreateBeamlarge");
	    					}
	    				}
    				}
    				disableButtons();
    				break;
    			case "Reload":
    				currentLevel = loader.load(currentLevel.path);
    				if (ui.getButton("CreateBeam") == null && currentLevel.smallBeamLimit > 0){
    		    		ui.addButton("res/buttons/psCreateBeam.png", "CreateBeam",
    		    				gcsave.getScreenWidth() - 75, 30, 50, 50);
    		            ui.getButton("CreateBeam").register(this);
    		    	}
    				if (ui.getButton("CreateBeamnormal") == null &&currentLevel.normalBeamLimit > 0){
    					ui.addButton("res/buttons/psCreateBeamnormal.png", "CreateBeamnormal",
    			        		gcsave.getScreenWidth() - 75, 100, 50, 50);
    			    	ui.getButton("CreateBeamnormal").register(this);
    				}
    				if(ui.getButton("CreateBeamlarge") == null && currentLevel.largeBeamLimit > 0) {
	    				ui.addButton("res/buttons/psCreateBeamlarge.png", "CreateBeamlarge",
	    			        	gcsave.getScreenWidth() - 75, 170, 50, 50);
	    			    ui.getButton("CreateBeamlarge").register(this);
    				}
    				break;
    		}
    	} catch (SlickException ignored) {}
    }
    
    public void reloadButtons(){
    	if (ui.getButton("CreateBeam") == null && currentLevel.smallBeamLimit > 0){
    		ui.addButton("res/buttons/psCreateBeam.png", "CreateBeam",
    				gcsave.getScreenWidth() - 75, 30, 50, 50);
            ui.getButton("CreateBeam").register(this);
    	}
		if (ui.getButton("CreateBeamnormal") == null &&currentLevel.normalBeamLimit > 0){
			ui.addButton("res/buttons/psCreateBeamnormal.png", "CreateBeamnormal",
	        		gcsave.getScreenWidth() - 75, 100, 50, 50);
	    	ui.getButton("CreateBeamnormal").register(this);
		}
		if(ui.getButton("CreateBeamlarge") == null && currentLevel.largeBeamLimit > 0) {
			ui.addButton("res/buttons/psCreateBeamlarge.png", "CreateBeamlarge",
		        	gcsave.getScreenWidth() - 75, 170, 50, 50);
		    ui.getButton("CreateBeamlarge").register(this);
		}
    }
    
    public void setLevel(String file) {
    	currentLevel = loader.load(System.getProperty("user.dir") + file);
    	if (currentLevel.largeBeamLimit == 0){
    		Button button = ui.getButton("CreateBeamlarge"); 
    		ui.addButton("res/buttons/psCreateBeamlarge2.png", "DontCreateBeam",
	        		button.getX(), button.getY(), 50, 50);
			ui.removeButton("CreateBeamlarge");
    	}
    	if (currentLevel.normalBeamLimit == 0){
    		Button button = ui.getButton("CreateBeamnormal"); 
    		ui.addButton("res/buttons/psCreateBeamnormal2.png", "DontCreateBeam",
	        		button.getX(), button.getY(), 50, 50);
			ui.removeButton("CreateBeamnormal");
    	}
    	if (currentLevel.smallBeamLimit == 0){
    		Button button = ui.getButton("CreateBeam"); 
    		ui.addButton("res/buttons/psCreateBeam2.png", "DontCreateBeam",
	        		button.getX(), button.getY(), 50, 50);
			ui.removeButton("CreateBeam");
    	}
    }
    
    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
    	gcsave = gc;
        ui = new UserInterface();
       	ui.addButton("res/buttons/psCreateBeam.png", "CreateBeam",
       			gc.getScreenWidth() - 75, 30, 50, 50);
        ui.getButton("CreateBeam").register(this);
    	ui.addButton("res/buttons/psCreateBeamnormal.png", "CreateBeamnormal",
        		gc.getScreenWidth() - 75, 100, 50, 50);
    	ui.getButton("CreateBeamnormal").register(this);
    	ui.addButton("res/buttons/psCreateBeamlarge.png", "CreateBeamlarge",
	        	gc.getScreenWidth() - 75, 170, 50, 50);
	    ui.getButton("CreateBeamlarge").register(this);
    	
    	ui.addButton("res/buttons/psReloadLevel.png", "Reload",
        		gc.getScreenWidth() - 75, gc.getScreenHeight() -75, 50, 50);
    	ui.getButton("Reload").register(this);
    	
    	in = new Input(gc.getScreenHeight());
        loader = new LevelLoader();
        //currentLevel = loader.load(System.getProperty("user.dir") + "/bin/res/level/Level1");
        //new Level("Test", "Test", -10f);
    }
    
    private String convSecondsToMinutes(int totalSeconds) {
    	int minutes, seconds;
    	minutes = totalSeconds / 60;
    	seconds = totalSeconds % 60;
    	return Integer.toString(minutes) + ":" + Integer.toString(seconds);
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
    	if (currentLevel == null) {
    		return;
    	}
    	currentLevel.render(g, gc);
        if (newBeam != null) {
        	newBeam.render(g);
        }
        
        g.drawString("Press <Space> to start/stopp.", 10, 30);
        if (currentLevel.existTimeLimit()){
        	if (currentLevel.checkTimeLimit()>0){
        		//g.drawString("Es bleiben noch "+(int)currentLevel.checkTimeLimit()+" Sekunden.",10,50);
        		g.drawString(convSecondsToMinutes((int)currentLevel.checkTimeLimit()),
        				gc.getScreenWidth()/2 - 10, 20);
        	}
        	else{
        		g.drawString("Verloren",gc.getScreenWidth()/2,gc.getScreenHeight()/2);
        	}
        }
        ui.draw(g);
        if (currentLevel.smallBeamLimit != 100 && currentLevel.smallBeamLimit != 0){
        	g.drawString(""+currentLevel.smallBeamLimit,gc.getScreenWidth() - 72, 62);
        }
        if (currentLevel.normalBeamLimit != 100 && currentLevel.normalBeamLimit != 0){
        	g.drawString(""+currentLevel.normalBeamLimit,gc.getScreenWidth() - 72, 132);
        }
        if (currentLevel.largeBeamLimit != 100 && currentLevel.largeBeamLimit != 0){
        	g.drawString(""+currentLevel.largeBeamLimit,gc.getScreenWidth() - 72, 202);
        }
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        if (in.isKeyDown(Input.KEY_SPACE)) {
        	//Wie man den Ball startet
        	if (ui.getClickTimeout() <= 0){
        		currentLevel.ChangeWorldMoves();
        	}
        	ui.setClickTimeout(100);
        }
        else if (in.isKeyDown(Input.KEY_LEFT)){
        	if (Marked != null && ui.getClickTimeout() <= 0){
        		Marked.setRotation(Marked.getRotation() - 2f);
        		//ui.setClickTimeout(10);
        	}
        } else if (in.isKeyDown(Input.KEY_RIGHT)) {
			if (Marked != null && ui.getClickTimeout() <= 0) {
				Marked.setRotation(Marked.getRotation() + 2f);
			}
			else{
				System.out.println(Marked);
			}
		}
        else if (in.isKeyDown(Input.KEY_ESCAPE)) {
            sbg.enterState(Program.MAIN_MENU_STATE);
        }
        else if (in.isKeyDown(Input.KEY_UP)){
        	currentLevel.VerschiebungVerringern();
        }
        else if (in.isKeyDown(Input.KEY_DOWN)){
        	currentLevel.VerschiebungErhöhen();
        }
        if (!currentLevel.CheckWon()){
        	currentLevel.checkTimeLimit(delta/1000f);
        }
        if (placingBeam && !currentLevel.CheckLost()) {
        	newBeam.setPosition(new Vec2(in.getAbsoluteMouseX(), in.getAbsoluteMouseY()-currentLevel.GetVerschiebung()));
        	if (in.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON) && ui.getClickTimeout() <= 0) {
        		currentLevel.addObject(newBeam);
        		newBeam.setLevel(currentLevel);
        		newBeam.attach();
        		Marked = null;
        		newBeam = null;
        		placingBeam = false;
        		
        		ui.setClickTimeout(250);
        		enableButtons();
        	}
        }
        if (!currentLevel.CheckLost()){
        	currentLevel.update(in, delta);
        }
        ui.update(in, delta);
    }

    @Override
    public int getID() {
        return Program.PLAY_STATE;
    }
}
