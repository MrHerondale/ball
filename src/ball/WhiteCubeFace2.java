package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class WhiteCubeFace2 extends WhiteCube{
	public WhiteCubeFace2 (float angle, Vec2 position, Level level) throws SlickException {
		super("res/cube2.png", angle, position, level);
	}
}
