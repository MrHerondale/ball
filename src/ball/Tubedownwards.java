package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class Tubedownwards extends Tube{
	public Tubedownwards (float restitution, Vec2 position, Level level) throws SlickException {
		super("res/Tube_Rechts.png", "res/Tube_front_Rechts.png", true , -10f, 150f, 200f, restitution, position, level);
	}
}
