package ball;

import java.util.Vector;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Button {

    protected Vector<ClickListener> listeners;
    protected Image img;
    protected int x, y;
    protected int width, height;
    protected String name;

    // x & y are top-left Screen Coordinates!
    public Button(String texture, String name, int x, int y, int width, int height) {
        try {
            img = new Image(texture);
        } catch (SlickException ex) {
            try {
                img = new Image("res/btnTextureNotFound.png");
                System.out.println("Could not load button-texture \"" + texture + "\"");
            } catch (SlickException ignored) {
                img = null;
            }
        }
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.name = name;
        this.listeners = new Vector<ClickListener>();
    }

    public String getName() {
        return name;
    }
    
    public int getX(){
    	return x;
    }

    
    public int getY(){
    	return y;
    }
    public void draw() {
        if (img != null) {
            img.draw(x, y);
        }
    }

    public void register(ClickListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void unregister(ClickListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    public boolean click(int clickX, int clickY) {
        if (clickX >= x && clickX <= x + width &&
                clickY >= y && clickY <= y + height) {
            for (int i = 0; i < listeners.size(); i++) {
                listeners.elementAt(i).click(this);
            }
            return true;
        }
        return false;
    }
}
