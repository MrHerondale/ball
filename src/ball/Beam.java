package ball;

import org.jbox2d.common.Vec2;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.SlickException;

public class Beam extends GameObject {

	public Beam (String texture, float width, float height, float angle,
				 float restitution, Vec2 position, Level level, boolean createBody) throws SlickException {
		super (texture, level, position, GameObject.STATIC_OBJECT, "Beam");

		PolygonShape shape = new PolygonShape();
		shape.setAsBox((width / 2) * Coords.SCALE, (height / 2) * Coords.SCALE,
				new Vec2(0, 0), (float)Math.toRadians(-angle));

		FixtureDef fixture = new FixtureDef();
		fixture.shape = shape;
		fixture.density = 0.5f;
		fixture.friction = 0.3f;
		fixture.restitution = restitution;

		super.img.setRotation(angle);
		super.setFixture(fixture);
	}
	
	public Beam (String texture, float width, float height, float angle,
			float restitution, Vec2 position, Level level) throws SlickException {
		this(texture, width, height, angle, restitution, position, level, true);
	}
}
