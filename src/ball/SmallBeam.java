package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class SmallBeam  extends Beam {
	public SmallBeam (float angle, Vec2 position, Level level) throws SlickException {
		super("res/beamFarbe.png", 100f, 25f, angle, 0.3f, position, level);
	}
}
