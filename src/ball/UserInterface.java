package ball;

import java.util.Vector;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class UserInterface {

    private Vector<Button> buttons;
    private int clicKTimeout = 0;

    public UserInterface() {
        buttons = new Vector<Button>();
    }

    public int getClickTimeout() {
    	return clicKTimeout;
    }
    
    public void setClickTimeout(int value) {
    	clicKTimeout = value;
    }
    
    public void addButton(Button button) {
        buttons.add(button);
    }

    public void addButton(String texture, String name, int x, int y, int width, int height) {
        addButton(new Button(texture, name, x, y, width, height));
    }

    public void removeButton(String name) {
    	Button b = getButton(name);
    	buttons.remove(b);
    }
    
    public void removeButton(Button button) {
    	buttons.remove(button);
    }
    
    public Button getButton(String name) {
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.elementAt(i).getName() == name) {
                return buttons.elementAt(i);
            }
        }
        return null;
    }

    public void draw(Graphics g) {
        for (int i = 0; i < buttons.size(); i++) {
            buttons.elementAt(i).draw();
        }
    }

    public void update(Input in, int delta) {
        int mouseX = in.getAbsoluteMouseX();
        int mouseY = in.getAbsoluteMouseY();
        if (clicKTimeout <= 0) {
            if (in.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
                clicKTimeout = 250;
                for (int i = 0; i < buttons.size(); i++) {
                    buttons.elementAt(i).click(mouseX, mouseY);
                }
            }
        } else {
            clicKTimeout -= delta;
        }
    }
}
