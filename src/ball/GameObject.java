package ball;
import org.newdawn.slick.Image;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.common.Vec2;

public class GameObject {
	public static final int STATIC_OBJECT = 1;
	public static final int DYNAMIC_OBJECT = 2;
	
	public String ObjectType = "";
	
	protected Image img;
	protected Body physBody;
	protected Level level;
	
	
	protected static final float SCALE = 0.01f;

	public GameObject(String texture, Level level_pass, Vec2 pos, int type, String Type, boolean createBody)
		throws SlickException {
		img = new Image(texture);
		img.setCenterOfRotation(img.getWidth() / 2, img.getHeight() / 2);
		level = level_pass;
		BodyDef def = new BodyDef();
		def.position.set(Coords.toPos(pos));
		ObjectType = Type;
		switch (type) {
			case STATIC_OBJECT:
				def.type = BodyType.STATIC;
				break;
			case DYNAMIC_OBJECT:
				def.type = BodyType.DYNAMIC;
				break;
		}
		if (createBody) {
			physBody = level.getWorld().createBody(def);
		}
	}

	public GameObject(String texture, Level level, Vec2 pos, int type, String Type) throws SlickException {
		this(texture, level, pos, type, Type, true);
	}

	public void setBody(Body b) {
		physBody = b;
	}
	
	public void setFixture(FixtureDef fixture)  {
		physBody.createFixture(fixture);
	}

	public void render(Graphics g) throws SlickException {
		Vec2 pos = physBody.getPosition();
		img.draw(Coords.toScreenX(pos.x) - img.getWidth() / 2, Coords.toScreenY(pos.y) - img.getHeight() / 2 + level.GetVerschiebung());
	}
	
	public void update(int delta) throws SlickException {
		
	}
}
