package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class LargeBeam extends Beam {
	public LargeBeam (float angle, Vec2 position, Level level) throws SlickException {
		super("res/beamlargeFarbe.png", 300f, 25f, angle, 0.3f, position, level);
	}
}
