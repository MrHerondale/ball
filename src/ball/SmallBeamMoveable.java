package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class SmallBeamMoveable extends MoveableBeam{
	public SmallBeamMoveable (boolean attached,float angle, Vec2 position, Level level) throws SlickException {
		super("res/beamFarbe.png","res/beamFarbe2.png", attached, 100f, 25f, angle, 0.3f, position, level);
	}
}
