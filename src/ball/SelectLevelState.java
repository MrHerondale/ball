package ball;

import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.Image;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.nio.file.Files;

public class SelectLevelState extends BasicGameState {
	
	private class LevelInfo {
		public String file;
		public String name;
		public String author;
		public Image img;
		public int x;
		public int y;
		public int width;
		public int height;
	}
	
	private static final String LEVEL_FILE_PREFIX = "/bin/res/level/";
	private static final String LEVEL_LIST_FILE = "/bin/res/level/levelList.txt";
	private LevelInfo[] levels;
	
	private int levelCount;
	private static int SPACING_X = 50;
	private static int SPACING_Y = 50;
	private int buttonsPerLine;
	private int buttonLines;
	
	private Input in;
	
	private boolean readLevelList() {
		try {
			BufferedReader r = Files.newBufferedReader(
					Paths.get(System.getProperty("user.dir") + LEVEL_LIST_FILE), 
					Charset.defaultCharset());
			levelCount = Integer.parseInt(r.readLine());
			levels = new LevelInfo[levelCount];
			for (int i = 0; i < levelCount; i++) {
				String line = r.readLine();
				String[] info = line.split(";");
				levels[i] = new LevelInfo();
				levels[i].file = info[0];
				levels[i].img = new Image(info[1]);
				levels[i].name = info[2];
				levels[i].author = info[3];
				levels[i].width = levels[i].img.getWidth();
				levels[i].height = levels[i].img.getHeight();
			}
		} catch (IOException ignored) {
			return false;
		} catch (SlickException ignored) {
			return false;
		}
		return true;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) {
		in = new Input(gc.getScreenHeight());
		
		readLevelList();
		// calculate menu layout
		int usableWidth = gc.getScreenWidth() - SPACING_X;
		buttonsPerLine = usableWidth / (levels[0].width + SPACING_X);
		if (buttonsPerLine > levelCount) {
			buttonsPerLine = levelCount;
		}
		buttonLines = ((levelCount % buttonsPerLine) == 0) ? levelCount / buttonsPerLine : levelCount / buttonsPerLine + 1;
		for (int x = 0; x < buttonsPerLine; x++) {
			for (int y = 0; y < buttonLines; y++) {
				if ((y * buttonsPerLine + x) >= levelCount) {
					break;
				}
				LevelInfo info = levels[y * buttonsPerLine + x];
				info.x = SPACING_X + x * SPACING_X + x * info.width;
				info.y = SPACING_Y + y * SPACING_Y + y * info.height;
			}
		}
	}
	
	public LevelInfo getInfo(int x, int y) {
		for (int i = 0; i < levelCount; i++) {
			if (x >= levels[i].x && x <= (levels[i].x + levels[i].width) &&
					y >= levels[i].y && y <= (levels[i].y + levels[i].height)) {
				return levels[i];
			}
		}
		return null;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		if (in.isKeyDown(Input.KEY_ESCAPE)) {
			sbg.enterState(Program.MAIN_MENU_STATE);
		}
		if (in.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
			LevelInfo info = getInfo(in.getMouseX(), in.getMouseY());
			if (info != null) {
				// Load Level
				PlayState ps = (PlayState)sbg.getState(Program.PLAY_STATE);
				ps.setLevel(LEVEL_FILE_PREFIX + info.file);
				ps.reloadButtons();
				sbg.enterState(Program.PLAY_STATE);
			}
		}
	}
	
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		for (int x = 0; x < buttonsPerLine; x++) {
			for (int y = 0; y < buttonLines; y++) {
				if ((y * buttonsPerLine + x) >= levelCount) {
					break;
				}
				LevelInfo info = levels[y * buttonsPerLine + x];
				info.img.draw(info.x, info.y);
				g.drawString(info.name + "\n" + info.author,
						info.x, info.y + info.height);
			}
		}
	}
	
	public int getID() {
		return Program.SELECT_LEVEL_STATE;
	}
}
