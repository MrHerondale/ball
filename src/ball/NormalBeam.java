package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class NormalBeam extends Beam{
	public NormalBeam (float angle, Vec2 position, Level level) throws SlickException {
		super("res/beamnormalFarbe.png", 200f, 25f, angle, 0.3f, position, level);
	}
}
