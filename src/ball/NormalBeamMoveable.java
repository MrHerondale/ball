package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class NormalBeamMoveable extends MoveableBeam{
	public NormalBeamMoveable (boolean attached,float angle, Vec2 position, Level level) throws SlickException {
		super("res/beamnormalFarbe.png","res/beamnormalFarbe2.png", attached, 200f, 25f, angle, 0.3f, position, level);
	}
}
