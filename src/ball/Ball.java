package ball;

import java.util.Vector;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.collision.shapes.CircleShape;
import org.newdawn.slick.SlickException;

public class Ball extends GameObject {
    public Ball(String texture, float radius, float restitution, Vec2 position, Level level) throws SlickException {
        super(texture, level, position, GameObject.DYNAMIC_OBJECT, "Ball");
        
        CircleShape shape = new CircleShape();
        shape.m_radius = radius * Coords.SCALE;
 
        FixtureDef fixture = new FixtureDef();
        fixture.density = 0.5f;
        fixture.friction = 0.3f;
        fixture.restitution = restitution;
        fixture.shape = shape;

        super.setFixture(fixture);
    }
    public boolean InAnyTube(Vector<GameObject> GameObjects) throws SlickException{
    	/*Tube t = (Tube)gameObjects.elementAt(i);
				t.render2(g);*/
    	for (int i = 0; i < GameObjects.size(); ++i){
    		if  (GameObjects.elementAt(i).ObjectType == "Tube"){
    			Tube t = (Tube)GameObjects.elementAt(i);
    			if (t.downwards == false){
    				if (t.InTube (this.physBody.getPosition())){
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }
}
