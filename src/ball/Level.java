package ball;
import java.util.Vector;
import org.newdawn.slick.Color;

import org.jbox2d.dynamics.World;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Graphics;

public class Level {

	private class CreateBody {
		public BodyDef body;
		public FixtureDef fixture;
		public GameObject gObj;
	}
	
	private World physWorld;
	private Vec2 gravity;
	private Vector<GameObject> gameObjects;
	private Vector<Body> destroyBodies;
	private Vector<CreateBody> newBodies;
	
	private int velocityIterations = 6;
	private int positionIterations = 2;

	private int clickTimeout = 0;
	// we need to keep track of the ball, to detect if it has reached the goal
	private int ballIndex = -1;

	private boolean WorldMoves = false;
	private boolean won = false;
	private boolean lost = false;
	private float Verschiebung = 0;
	private float limit;
	private float timeLimit = 0;
	private boolean timeSet = false;
	public String path = "";
	public float windowsave;
	public int smallBeamLimit = 100;
	public int normalBeamLimit = 100;
	public int largeBeamLimit = 100;
	
	public boolean CheckLost(){
		return lost;
	}
	
	public boolean CheckWon(){
		return won;
	}
	
	public void SetTimeLimit (float newTimeLimit){
		if (newTimeLimit > 0){
			timeSet = true;
			timeLimit = newTimeLimit;
		}
	}
	
	public boolean existTimeLimit(){
		return timeSet;
	}
	
	public float checkTimeLimit(float Time){
		if (timeSet){
			timeLimit -= Time;
			if (timeLimit <= 0){
				lost = true;
			}
		}
		return timeLimit;
	}
	
	public float checkTimeLimit(){
		return checkTimeLimit(0f);
	}
	
	public Level(String name, String author, float gravity, String path) {
		this.gravity = new Vec2(0f, gravity);
		this.path = path;
		// doSleep = true, allows bodies to rest (increasing performance)
		physWorld = new World(this.gravity, true); 
		
		gameObjects = new Vector<GameObject>();
		destroyBodies = new Vector<Body>();
		newBodies = new Vector<CreateBody>();
	}
	
	public void SetLimit(float newLimit){
		limit = newLimit;
	}
	
	public float GetLimit(){
		return limit;
	}
	
	public void MoveWorld (){
		WorldMoves = true;
	}
	
	public void StopWorld (){
		WorldMoves = false;
	}
	
	public void ChangeWorldMoves(){
		if (WorldMoves == true){
			StopWorld();
		}
		else{
			MoveWorld();
		}
	}
	
	public void addObject(GameObject object) {
		gameObjects.add(object);
	}

	public void addBall(Ball ball) {
		gameObjects.add(ball);
		ballIndex = gameObjects.indexOf(ball);
	}

	public World getWorld() {
		return physWorld;
	}
	
	private void handleInput(Input in) {
		if (in.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON) && clickTimeout <= 0) {
			// 
		}
	}
	
	private void destroyBodies() {
		if (destroyBodies.size() > 0) {
			for (Body body:destroyBodies) {
				physWorld.destroyBody(body);
			}
			destroyBodies.clear();
		}
	}
	
	private void createBodies() {
		if (newBodies.size() > 0) {
			for (CreateBody newB:newBodies) {
				Body b = physWorld.createBody(newB.body);
				b.createFixture(newB.fixture);
				newB.gObj.setBody(b);
			}
			newBodies.clear();
		}
	}
	
	public void update(Input in, int delta) throws SlickException {
		
		handleInput(in);
		
		for (int i = 0; i < gameObjects.size(); i++) {
			gameObjects.elementAt(i).update(delta);
			if (won == false){
				if (gameObjects.elementAt(i).ObjectType == "Ball" && !lost){
					if (((Ball)gameObjects.elementAt(i)).InAnyTube(gameObjects)){
						won = true;
					}
				}
			}
		}
		
		clickTimeout = (clickTimeout > 0) ? clickTimeout - delta : clickTimeout;
		float timeStep = delta / 1000f;
		//Hier ist die Stelle, wo die Objecte bewegt werden.
		
		if (WorldMoves){
			physWorld.step(timeStep, velocityIterations, positionIterations);
			destroyBodies();
			createBodies();
		}
	}
	
	public void render(Graphics g, GameContainer gc) throws SlickException {
		windowsave = gc.getScreenWidth();
		for (int i = 0; i < gameObjects.size(); i++) {
			if (gameObjects.elementAt(i).ObjectType == "Tube"){
				Tube t = (Tube)gameObjects.elementAt(i);
				t.render2(g);
			}
		}
		for (int i = 0; i < gameObjects.size(); i++) {
			gameObjects.elementAt(i).render(g);
		}
		if (won){
			g.drawString("GEWONNEN", gc.getScreenWidth()/2, gc.getScreenHeight()/2);
			//g.drawString(20.0f, 20.0f, "this is a test", Color.gray);
		}
	}
	
	public void destroyBody(Body body) {
		destroyBodies.add(body);
	}
	
	public void createBody(GameObject g, BodyDef body, FixtureDef fixture) {
		CreateBody foo = new CreateBody();
		foo.body = body;
		foo.fixture = fixture;
		foo.gObj = g;
		newBodies.add(foo);
	}
	public void VerschiebungErhöhen(){
		if (Verschiebung > limit){
			Verschiebung -=2;
		}
	}
	public void VerschiebungVerringern(){
		if (Verschiebung < 0){
			Verschiebung +=2;
		}
	}
	public float GetVerschiebung(){
		return Verschiebung;
	}
}
