package ball;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Image;
import org.newdawn.slick.Graphics;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.FixtureDef;

public class MoveableBeam extends Beam {
	private boolean attached;
	private Vec2 position;
	private float angle;
	//private Level level;
	private float width;
	private float height;
	private float restitution;
	
	private Image detachedImage;
	
	public MoveableBeam(String attachedTexture, String detachedTexture,
			boolean attached, float width, float height, float angle,
			float restitution, Vec2 position, Level level) throws SlickException {
		super(attachedTexture, width, height, angle, restitution, position, level);
		
		this.attached = attached;
		this.level = level;
		this.position = Coords.toPos(position);
		this.angle = angle;
		this.width = width;
		this.height = height;
		this.restitution = restitution;
		
		detachedImage = new Image(detachedTexture);
		detachedImage.setCenterOfRotation(width / 2, height / 2);
		detachedImage.setRotation(angle);

		/*
		if (!attached) {
			// delete the previously created physBody
			level.destroyBody(super.physBody);
		} */
	}
	
	public float getRotation() {
		return angle;
	}
	
	public void setLevel(Level value) {
		level = value;
	}
	
	public void setRotation(float value) {
		angle = value;
		img.setRotation(angle);
		detachedImage.setRotation(angle);
		if (attached) {
			physBody.setTransform(physBody.getPosition(), (float)Math.toRadians(-angle));
		}
	}
	
	public Vec2 getPosition() {
		return Coords.toScreen(position);
	}
	
	public void setPosition(Vec2 value) {
		position = Coords.toPos(value);
	}
	
	public void detach() {
		if (attached) {
			attached = false;
			position = super.physBody.getPosition();
			level.destroyBody(super.physBody);
		}
	}
	
	public void attach() {
		physBody.setTransform(position, (float)Math.toRadians(-angle));
		attached = true;
	}
	
	@Override
	public void render(Graphics g) {
		if (attached) {
			super.img.draw(Coords.toScreenX(position.x) - img.getWidth() / 2,
					Coords.toScreenY(position.y) - img.getHeight() / 2 + level.GetVerschiebung());
		} else {
			detachedImage.draw(Coords.toScreenX(position.x) - img.getWidth() / 2, 
					Coords.toScreenY(position.y) - img.getHeight() / 2 + level.GetVerschiebung());
		}
	}
}
