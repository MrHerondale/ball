package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class Tubenormal extends Tube{
	public Tubenormal (float restitution, Vec2 position, Level level) throws SlickException {
		super("res/Tube_Links.png", "res/Tube_front_Links.png", false , -10f, 150f, 200f, restitution, position, level);
	}
}
