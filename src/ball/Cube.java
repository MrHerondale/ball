package ball;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.SlickException;

public class Cube extends GameObject {
	public Cube (String texture, float width, float angle,
			 float restitution, Vec2 position, Level level) throws SlickException {
	super (texture, level, position, GameObject.DYNAMIC_OBJECT, "Cube");

	PolygonShape shape = new PolygonShape();
	shape.setAsBox((width / 2) * Coords.SCALE, (width / 2) * Coords.SCALE,
			new Vec2(0, 0), (float)Math.toRadians(-angle));

	FixtureDef fixture = new FixtureDef();
	fixture.shape = shape;
	fixture.density = 0.4f;
	fixture.friction = 0.3f;
	fixture.restitution = restitution;

	super.img.setRotation(angle);
	super.setFixture(fixture);
}
}
