package ball;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenuState extends BasicGameState implements ClickListener {

    private Input in;
    private UserInterface ui;
    private StateBasedGame game;

    public MainMenuState() {
        ui = new UserInterface();

    }

    @Override
    public  void click(Button button) {
        switch (button.getName()) {
            case "StartGame":
                game.enterState(Program.SELECT_LEVEL_STATE);
                break;
            case "EndGame":
                game.enterState(Program.EXIT_STATE);
                break;
        }
     }

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        game = sbg;
        in = new Input(gc.getScreenHeight());
        ui.addButton("res/buttons/mmStartGame.png", "StartGame",
                gc.getScreenWidth() / 2 - 75, gc.getScreenHeight() / 2,
                150, 75);
        ui.getButton("StartGame").register(this);
        ui.addButton("res/buttons/mmEndGame.png", "EndGame",
                gc.getScreenWidth() / 2 - 75, gc.getScreenHeight() / 2 + 125,
                150, 75);
        ui.getButton("EndGame").register(this);
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
        ui.draw(g);
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        ui.update(in, delta);
    }

    @Override
    public int getID() {
        return Program.MAIN_MENU_STATE;
    }
}
