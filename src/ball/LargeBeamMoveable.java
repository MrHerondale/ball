package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class LargeBeamMoveable extends MoveableBeam{
	public LargeBeamMoveable (boolean attached,float angle, Vec2 position, Level level) throws SlickException {
		super("res/beamlargeFarbe.png","res/beamlargeFarbe2.png", attached, 300f, 25f, angle, 0.3f, position, level);
	}
}
