package ball;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Tube extends GameObject{
	
	public boolean downwards = false;
	private String texture = "";
	private Image img2;
	private float left;
	private float right;
	private float up;
	private float down;
	private float fix2;
	
	public Tube (String texture2, String texture1, boolean downwardsbo, float fix, float width, float height,
			float restitution, Vec2 position, Level level) throws SlickException {
		super (texture1, level, position, GameObject.STATIC_OBJECT, "Tube");
		fix2 = fix;
		downwards = downwardsbo;
		texture = texture2;
		left = position.x - (width/2);
		right = position.x + (width/2);
		up = position.y - (height/2);
		down = position.y + (height/2);
		
		img2 = new Image(texture);
		img2.setCenterOfRotation(img2.getWidth() / 2, img2.getHeight() / 2);
		if (downwards){
			img2.setRotation(180f);
			fix2 = 0f;
		}
		
		PolygonShape shape = new PolygonShape();
		//shape.setAsBox((width / 2) * Coords.SCALE, (height / 2) * Coords.SCALE, new Vec2(0, 0), 0f);
		shape.setAsBox(((height/2)+fix)*Coords.SCALE, 2*Coords.SCALE, new Vec2((-(width/2)+1)*Coords.SCALE,fix2*Coords.SCALE),
				(float)Math.toRadians(90));
		FixtureDef fixture = new FixtureDef();
		fixture.shape = shape;
		fixture.density = 0.5f;
		fixture.friction = 0.3f;
		fixture.restitution = restitution;
		
		PolygonShape shape2 = new PolygonShape();
		shape2.setAsBox(((height/2)+fix)*Coords.SCALE, 2*Coords.SCALE, new Vec2(((width/2)-1)*Coords.SCALE,fix2*Coords.SCALE),
				(float)Math.toRadians(90));
		
		FixtureDef fixture2 = new FixtureDef();
		fixture2.shape = shape2;
		fixture2.density = 0.5f;
		fixture2.friction = 0.3f;
		fixture2.restitution = restitution;
		
		PolygonShape shape3 = new PolygonShape();
		float downposition = (-(height/2)-1-fix2);
		if (downwards){
			downposition = ((height/2)-1-fix2);
		}
		shape3.setAsBox((width/2)*Coords.SCALE, 2*Coords.SCALE, new Vec2(0,downposition*Coords.SCALE),0);
		
		FixtureDef fixture3 = new FixtureDef();
		fixture3.shape = shape3;
		fixture3.density = 0.5f;
		fixture3.friction = 0.3f;
		fixture3.restitution = restitution;
		
		if (downwards){
			super.img.setRotation(180f);
		}
		else{
			super.img.setRotation(0f);
		}
		super.setFixture(fixture);
		super.setFixture(fixture2);
		super.setFixture(fixture3);
	}
	public String get_texture (){
		return texture;
	}
	public void render2 (Graphics g){
		Vec2 pos = physBody.getPosition();
		img2.draw(Coords.toScreenX(pos.x) - img2.getWidth() / 2, Coords.toScreenY(pos.y) - img2.getHeight() / 2 + level.GetVerschiebung());
	}
	public boolean InTube (Vec2 Point){
		if (left >= Coords.toScreenX(Point.x) || right <= Coords.toScreenX(Point.x)){
			return false;
		}
		else if (up-fix2 >= Coords.toScreenY(Point.y) || down <= Coords.toScreenY(Point.y)){
			return false;
		}
		return true;
	}
}
