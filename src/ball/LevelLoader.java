package ball;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class LevelLoader {

    private Level newLevel = null;

    public Level load(String path) {
        Path p = Paths.get(path);
        if (!Files.exists(p)) {
            System.out.println(path);
            return null;
        }

        try {
            BufferedReader br = Files.newBufferedReader(p, Charset.defaultCharset());
            parseLevelHeader(br, path);
            parseObjectList(br);
        } catch (IOException ex) {

        }
        return newLevel;
    }

    /* Each level file must begin with the following Header:
     * Name = <Name Of Level>
     * Author = <Name Of Author>
     * Gravity = <FLOAT_VALUE>
     * <BLANK LINE>
     */
    private void parseLevelHeader(BufferedReader br, String path) throws IOException {
        String author = "";
        String name = "";
        float gravity = 0f;
        float limit = -120;
    	float timeLimit = 0f;
    	int smallBeamLimit = 100;
    	int normalBeamLimit = 100;
    	int largeBeamLimit = 100;
        while (true) {
            String line = br.readLine();
            if (line == null || line.isEmpty()) {
                break;
            }
            if (line.startsWith("Name = ")) {
                name = line.substring("Name = ".length());
            } else if (line.startsWith("Author = ")) {
                author = line.substring("Author = ".length());
            } else if (line.startsWith("Gravity = ")) {
                gravity = Float.parseFloat(line.substring("Gravity = ".length()));
            } else if (line.startsWith("Limit = ")){
            	limit = Float.parseFloat(line.substring("Limit = ".length()));
            } else if (line.startsWith("Timelimit = ")){
            	timeLimit = Float.parseFloat(line.substring("Timelimit = ".length()));
            }
            else if (line.startsWith("SmallBeamLimit = ")){
            	smallBeamLimit = (int)Float.parseFloat(line.substring("SmallBeamLimit = ".length()));
            }
            else if (line.startsWith("NormalBeamLimit = ")){
            	normalBeamLimit = (int)Float.parseFloat(line.substring("NormalBeamLimit = ".length()));
            }
            else if (line.startsWith("LargeBeamLimit = ")){
            	largeBeamLimit = (int)Float.parseFloat(line.substring("LargeBeamLimit = ".length()));
            }
        }
        newLevel = new Level(name, author, gravity, path);
        newLevel.SetLimit(limit);
        newLevel.SetTimeLimit(timeLimit);
        newLevel.smallBeamLimit = smallBeamLimit;
        newLevel.normalBeamLimit = normalBeamLimit;
        newLevel.largeBeamLimit = largeBeamLimit;
    }

    private Vec2 parseVec2(String vecStr) {
        String xStr;
        String yStr;
        int i = 0;
        while (vecStr.charAt(i) != ';' && i < vecStr.length()) {
            i++;
        }
        xStr = vecStr.substring(0, i);
        yStr = vecStr.substring(i+1);
        float x = Float.parseFloat(xStr);
        float y = Float.parseFloat(yStr);
        Vec2 vec = new Vec2(x, y);
        return vec;
    }

    /* Ball Info is provided in a single line:
     * Ball "Texture" Radius Position Restitution
     *
     * Radius is a float
     * Position is a 2D-Vector encoded as:
     * x;y
     * Restitution is a float
     */
    
    public void NewBalls(BufferedReader br) throws IOException {
    	while (true) {
            String line = br.readLine();
            if (line == null || line.isEmpty()) {
                break;
            }
            if (line.startsWith("Ball ")) {
                parseBallInfo(line.substring("Ball ".length()), br);
            }
    	}
    }
    
    private void parseBallInfo(String line, BufferedReader br) throws IOException {
        String texture = "";
        if (line.charAt(0) != '"') {
            return;
        }
        int i = 1;
        while (line.charAt(i) != '"') {
            texture += line.charAt(i);
            i++;
        }
        i += 2; // skip space
        String radiusString = "";
        float radius;
        while (line.charAt(i) != ' ') {
            radiusString += line.charAt(i);
            i++;
        }
        radius = Float.parseFloat(radiusString);
        i += 1;
        String positionString = "";
        Vec2 position;
        while (line.charAt(i) != ' ') {
            positionString += line.charAt(i);
            i++;
        }
        position = parseVec2(positionString);
        i += 1; // skip space
        String restitutionString = "";
        float restitution;
        for (; i < line.length(); i++) {
            restitutionString += line.charAt(i);
        }
        restitution = Float.parseFloat(restitutionString);
        try {
            Ball ball = new Ball(texture, radius, restitution, position, newLevel);
            newLevel.addBall(ball);
        } catch (SlickException ex) {
            throw new IOException("Error while parsing Ball info");
        }
    }
    
    private void parseTube(String type, String line, BufferedReader br) throws IOException{
    	int i = 0;
        String positionString = "";
        Vec2 position;
        while (i < line.length()) {
            positionString += line.charAt(i);
            i++;
        }
        i += 1;
        position = parseVec2(positionString);
        try {
            switch (type) {
                case "Normal":
                	Tubenormal tube = new Tubenormal(0.3f, position, newLevel);
                    newLevel.addObject(tube);
                    float Abstand = position.y;
                    while (Abstand < (-newLevel.GetLimit())+750){
                    	Abstand += 150;
                    	Vec2 position2 = new Vec2 (position.x, Abstand);
                    	Tubenormal NewTube = new Tubenormal(0.3f, position2, newLevel);
                    	newLevel.addObject(NewTube);
                    }
                	break;
                case "Downwards":
                	Tubedownwards tube2 = new Tubedownwards(0.3f, position, newLevel);
                    newLevel.addObject(tube2);
                    float Abstand2 = position.y;
                    while (Abstand2 > 0){
                    	Abstand2 -= 150;
                    	Vec2 position2 = new Vec2 (position.x, Abstand2);
                    	Tubedownwards NewTube = new Tubedownwards(0.3f, position2, newLevel);
                    	newLevel.addObject(NewTube);
                    }
                	break;
            }
        } catch (SlickException ex) {
            throw new IOException("Error while parsing Tube info");
        }
    }
    
    private void parseBeam(String type, String line, BufferedReader br) throws IOException {
        int i = 0;
        String rotationString = "";
        float rotation;
        while (line.charAt(i) != ' ') {
            rotationString += line.charAt(i);
            i++;
        }
        i += 1;
        rotation = Float.parseFloat(rotationString);
        String positionString = "";
        Vec2 position;
        while (i < line.length()) {
            positionString += line.charAt(i);
            i++;
        }
        position = parseVec2(positionString);
        try {
            switch (type) {
                case "Small":
                    SmallBeam beam = new SmallBeam(rotation, position, newLevel);
                    newLevel.addObject(beam);
                    break;
                case "Normal":
                	NormalBeam beam2 = new NormalBeam(rotation, position, newLevel);
                    newLevel.addObject(beam2);
                	break;
                case "Large":
                	LargeBeam beam3 = new LargeBeam(rotation, position, newLevel);
                    newLevel.addObject(beam3);
                	break;
            }
        } catch (SlickException ex) {
            throw new IOException("Error while parsing Beam info");
        }
    }

    private void parseCube(String type, String line, BufferedReader br) throws IOException {
        int i = 0;
        String rotationString = "";
        float rotation;
        while (line.charAt(i) != ' ') {
            rotationString += line.charAt(i);
            i++;
        }
        i += 1;
        rotation = Float.parseFloat(rotationString);
        String positionString = "";
        Vec2 position;
        while (i < line.length()) {
            positionString += line.charAt(i);
            i++;
        }
        position = parseVec2(positionString);
        try {
            switch (type) {
                case "2":
                    WhiteCubeFace2 cube = new WhiteCubeFace2(rotation, position, newLevel);
                    newLevel.addObject(cube);
                    break;
            }
        } catch (SlickException ex) {
            throw new IOException("Error while parsing Cube info");
        }
    }
    
    private void parseObjectList(BufferedReader br) throws IOException {
        while (true) {
            String line = br.readLine();
            if (line == null || line.isEmpty()) {
                break;
            }

            if (line.startsWith("Ball ")) {
                parseBallInfo(line.substring("Ball ".length()), br);  
            } else if (line.startsWith("Small Beam ")) {
                parseBeam("Small", line.substring("Small Beam ".length()), br);
            } else if (line.startsWith("Normal Beam ")) {
                parseBeam("Normal", line.substring("Normal Beam ".length()), br);
            } else if (line.startsWith("Large Beam ")) {
                parseBeam("Large", line.substring("Large Beam ".length()), br);
            } else if (line.startsWith("Tube Normal")) {
                parseTube("Normal", line.substring("Tube Normal".length()), br);
            } else if (line.startsWith("Tube Downwards ")) {
                parseTube("Downwards", line.substring("Tube Downwards ".length()), br);
            } else if (line.startsWith("Cube 2 ")) {
                parseCube("2", line.substring("Cube 2 ".length()), br);
            }
        }
    }
}
