package ball;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;

public class WhiteCube extends Cube {
	public WhiteCube (String texture, float angle, Vec2 position, Level level) throws SlickException {
		super(texture, 30f, angle, 0.2f, position, level);
	}
}
