package ball;

/* Objects that want to be notified
 * when a Button was clicked must implement this interface
 */
public interface ClickListener {
    public void click(Button button);
}
