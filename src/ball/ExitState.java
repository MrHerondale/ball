package ball;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class ExitState extends BasicGameState {
    public ExitState() {

    }

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) {
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) {
        gc.exit();
    }

    @Override
    public  void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
    }

    @Override
    public int getID() {
        return Program.EXIT_STATE;
    }
}
