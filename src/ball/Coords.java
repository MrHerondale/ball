package ball;

import org.jbox2d.common.Vec2;

public class Coords {

	// 100 pixel = 1 m
	public static final float SCALE = 0.01f;
	// Convert screen Coordinates to Physic World Coordinates
	public static float toPosX(float posX) {
		float x = posX * SCALE;
		return x;
	}
	
	public static float toPosY(float posY) {
		float y = (-posY * SCALE);
		return y;
	}
	
	public static float toScreenX(float posX) {
		float x = posX / SCALE;
		return x;
	}
	
	public static float toScreenY(float posY) {
		float y = (-posY / SCALE);
		return y;
	}
	
	public static Vec2 toPos(Vec2 vec) {
		return new Vec2(toPosX(vec.x), toPosY(vec.y));
	}
	
	public static Vec2 toScreen(Vec2 vec) {
		return new Vec2(toScreenX(vec.x), toScreenY(vec.y));
	}
}
