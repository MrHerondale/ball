package ball;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.GameContainer;
//import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

//import org.newdawn.slick.Image;

public class Program extends StateBasedGame
{

	public static final int PLAY_STATE = 0;
	public static final int MAIN_MENU_STATE = 1;
	public static final int SELECT_LEVEL_STATE = 2;
	public static final int EXIT_STATE = -1;

	public Program(String gamename) {
		super(gamename);
		super.addState(new PlayState());
		super.addState(new MainMenuState());
		super.addState(new SelectLevelState());
		super.addState(new ExitState());
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(PLAY_STATE).init(gc, this);
		this.getState(MAIN_MENU_STATE).init(gc, this);
		this.getState(SELECT_LEVEL_STATE).init(gc, this);
		this.enterState(MAIN_MENU_STATE);
	}

	public static void main(String[] args) {
		try {
			AppGameContainer appgc;
			appgc = new AppGameContainer(new Program("Simple Slick Game"));
			appgc.setDisplayMode(appgc.getScreenWidth(), appgc.getScreenHeight(), true);
			appgc.start();
		}
		catch (SlickException ex) {
			Logger.getLogger(Program.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}