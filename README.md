# README #

Beschäftigungstherapie für zwischendurch.

### Dieses Repository ###

* Auf diesem Repository findet die Entwicklung des Spiels statt,
die jeweilige Entwicklerversion kann durch klonen des Repositories heruntergeladen werden.
* Die aktuelle Stabile Version findet sich unter Downloads

### Setup ###

* Stabile Version: zip-File herunterladen, entpacken, run.bat / run.sh ausführen.

* Entwicklerversion: Repository klonen, Eclipse / IntelliJ Projekt erzeugen, notwendige Abhängigkeiten (slick2d und jbox2d-2.12) hinzufügen und kompilieren

### Ich möchte mich beteiligen! ###

Sehr gern gesehen sind Bug-Reports (oder Fixes!) und neue oder überarbeitete Level

### Kontakt ###

Kontakt über greenfoxlight.42@gmail.com oder über den Issue-Tracker